# Herbify
![alt text](Screenshots/logo.png)

The Herbify app offers smart greenhouses for small plants. Controlled by Android the greenhouses can be monitored and actions can be performed or programmed.

## Screenshots

* *Main Page list view*

![alt text](Screenshots/main.png)

* *Main page square view*

![alt text](Screenshots/newView.png)

* *Plant view*

![alt text](Screenshots/plantView.png)

* *Info and Actions*

![alt text](Screenshots/ActuatorsInfo.png)

* *Electronics diagram*

![alt text](Screenshots/Diagram.png)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* If you like this project and you want to help me please contact me at r.seara at campus dot fct dot unl dot pt

