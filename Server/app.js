
var express = require('express');
var app = express();
var fs = require("fs");
var net = require('net');
var bodyParser = require('body-parser')

app.use(bodyParser.json());
app.use(bodyParser.text());



//var clients = new WeakMap;
//clients.set(ghId, Socket); 
//clients.get(ghId, Socket); 
//clients.delete(ghId, Socket); 

var client = new net.Socket();

var WifiIP;


app.get('/', function(req, res) {
  res.send('RESTful API - Herbify');
});


// GET METHODS
app.post('/api/auth', function(req, res, next) {
  fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {

    data = JSON.parse(data);
    console.log(data.users);

    var count = Object.keys(data.users).length;

    var body = req.body;
    var email = body.username;
    var pass = body.password;

    console.log(email);

    console.log(pass);

    for (var i = 0; i < count; i++) {
      console.log(data.users[i].name);
      console.log(data.users[i].email);
      console.log(data.users[i].password);

      if(String(data.users[i].email) == String(email) && String(data.users[i].password) == String(pass)){
        console.log("USER LOGGIN: " + data.users[i].name);
        var obj = new Object();
        obj.id = i;
        obj.username  = data.users[i].name;
        var jsonString= JSON.stringify(obj);
        res.send(jsonString);
        return;
      }
    };

    console.log("USER BAD LOGGIN");
    var obj = new Object();
    obj.id = -1;
    obj.username  = "NO USER";
    var jsonString= JSON.stringify(obj);
    res.send(jsonString);
  });
})

// Definir um endpoint da API
app.get('/api/listAll', function(req, res, next) {
  fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
    console.log("GET List All REQUEST");
    res.send(data);
  });
})


// details of 1 user
app.get('/api/detailsUser/:userId([0-9]+)', function(req, res, next) {
  fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
    data = JSON.parse(data);
    var userId = req.params.userId;
    console.log("GET 1 User Details");
    res.send(data.users[userId]);
  });
})


//greenhouses of 1 user
//app.get('/api/listGH/:userId([0-9]+)', function(req, res, next) {
  app.get('/api/detailsUser/:userId([0-9]+)/listGH', function(req, res, next) {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
      data = JSON.parse(data);
      var userId = req.params.userId;
      console.log("GET 1 User GH's");
      res.send(data.users[userId].estufas);
    });
  })


// 1 greenhouse details
app.get('/api/detailsUser/:userId([0-9]+)/listGH/:ghId([0-9]+)', function(req, res, next) {
  fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
    data = JSON.parse(data);
    var userId = req.params.userId;
    var ghId = req.params.ghId;
    console.log("GET 1 GH details");
    res.send(data.users[userId].estufas[ghId]);
  });
})



app.get('/api/detailsUser/:userId([0-9]+)/listGH/:ghId([0-9]+)/actuators', function(req, res, next) {
  fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
    data = JSON.parse(data);
    var userId = req.params.userId;
    var ghId = req.params.ghId;
    console.log("GET 1 GH details");
    res.send(data.users[userId].estufas[ghId].actuators);
  });
})


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// POST METHODS


/*
    client.write();
    client.destroy();
*/

//Definir WIFI IP
app.post('/api/wifiId', function(req, res, next) {
  WifiIP = req.body;
  console.log(req.body);


  client.connect(3300, WifiIP, function() {
    console.log('Connected');
  });

  console.log("WIFI UPDATED");
  res.send("WIFI UPDATED");
})

// Force MR. Arduino to share the values
app.post('/api/updateSignalGH/:userId([0-9]+)/:ghId([0-9]+)', function(req, res, next) {
  if (typeof client.remoteAddress !== "undefined") {
    client.write('4');
    res.send({"result " : "Arduino request sent"});
  }else{
    res.send("ERROR: Greenhouse not available");
  }
})


// LIGHT STATE
app.post('/api/detailsUser/:userId([0-9]+)/listGH/:ghId([0-9]+)/light/state=:stateVal([0-1])/', function(req, res, next) {
 console.log("Connection " + client.remoteAddress);
 console.log("Connection " + client.connecting);

  if (typeof client.remoteAddress !== "undefined") {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
      data = JSON.parse(data);
      var userId = req.params.userId;
      var ghId = req.params.ghId;
      var stateVal = req.params.stateVal;
      data.users[userId].estufas[ghId].actuators.light =  stateVal;
      console.log("LIGHT STATE: " + stateVal);

      if (stateVal == 1){
        client.write('1');
        console.log("Sent Signal on socket ");
      }else{
        client.write('0');
        console.log("Sent Signal on socket ");
      }

      fs.writeFile( __dirname + "/" + "data.json", JSON.stringify(data, null, 4), function (err) {
        if (err) return console.log(err);
      });
      res.send(data.users[userId].estufas[ghId].actuators);

    });
  }else{
    res.send("ERROR: Greenhouse not available");
  }
})

// FAN STATE
app.post('/api/detailsUser/:userId([0-9]+)/listGH/:ghId([0-9]+)/fan/state=:stateVal([0-1])/', function(req, res, next) {
 console.log("Connection " + client.remoteAddress);
 console.log("Connection " + client.connecting);
  if (typeof client.remoteAddress !== "undefined") {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
      data = JSON.parse(data);
      var userId = req.params.userId;
      var ghId = req.params.ghId;
      var stateVal = req.params.stateVal;
      data.users[userId].estufas[ghId].actuators.fan =  stateVal;
      console.log("FAN STATE: " + stateVal);

      if (stateVal == 1){
        client.write('k');
        console.log("Sent Signal on socket ");
      }else{
        client.write('x');
        console.log("Sent Signal on socket ");
      }

      fs.writeFile( __dirname + "/" + "data.json", JSON.stringify(data, null, 4), function (err) {
        if (err) return console.log(err);
      });
      res.send(data.users[userId].estufas[ghId].actuators);

    });
  }else{
    res.send("ERROR: Greenhouse not available");
  }
})


// WINDOW STATE
app.post('/api/detailsUser/:userId([0-9]+)/listGH/:ghId([0-9]+)/window/state=:stateVal([0-1])/', function(req, res, next) {
 console.log("Connection " + client.remoteAddress);
 console.log("Connection " + client.connecting);

  if (typeof client.remoteAddress !== "undefined") {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
      data = JSON.parse(data);
      var userId = req.params.userId;
      var ghId = req.params.ghId;
      var stateVal = req.params.stateVal;
      data.users[userId].estufas[ghId].actuators.window =  stateVal;
      console.log("WINDOW STATE: " + stateVal);

      if (stateVal == 1){
        client.write('3');
        console.log("Sent Signal on socket ");

      }else{
        client.write('2');
        console.log("Sent Signal on socket ");
      }

      fs.writeFile( __dirname + "/" + "data.json", JSON.stringify(data, null, 4), function (err) {
        if (err) return console.log(err);
      });
      res.send(data.users[userId].estufas[ghId].actuators);
    });
  }else{
    res.send("ERROR: Greenhouse not available");
  }
})

// WATER STATE 
app.post('/api/detailsUser/:userId([0-9]+)/listGH/:ghId([0-9]+)/water/state=:stateVal([0-1])/', function(req, res, next) {
 console.log("Connection " + client.remoteAddress);
 console.log("Connection " + client.connecting);

  if (typeof client.remoteAddress !== "undefined") {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
      data = JSON.parse(data);
      var userId = req.params.userId;
      var ghId = req.params.ghId;
      var stateVal = req.params.stateVal;
      data.users[userId].estufas[ghId].actuators.water =  stateVal;
      console.log("WATER STATE: " + stateVal);

      if (stateVal == 1){
            client.write('y');
            console.log("Sent Signal on socket ");

          }else{
            client.write('z');
            console.log("Sent Signal on socket ");
          }

          fs.writeFile( __dirname + "/" + "data.json", JSON.stringify(data, null, 4), function (err) {
            if (err) return console.log(err);
          });
          res.send(data.users[userId].estufas[ghId].actuators);
        });
  }else{
    res.send("ERROR: Greenhouse not available");
  }
})

// MANUAL WINDOW STATE
app.post('/api/detailsUser/:userId([0-9]+)/listGH/:ghId([0-9]+)/manualWindow/state=:stateVal([0-1])/', function(req, res, next) {
 console.log("Connection " + client.remoteAddress);
 console.log("Connection " + client.connecting);

  if (typeof client.remoteAddress !== "undefined") {

    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
      data = JSON.parse(data);
      var userId = req.params.userId;
      var ghId = req.params.ghId;
      var stateVal = req.params.stateVal;
      data.users[userId].estufas[ghId].actuators.mw =  stateVal;
      console.log("MANUAL WINDOW STATE: " + stateVal);

      if (stateVal == 1){
        client.write('5');
        console.log("Sent Signal on socket ");

      }else{
        client.write('6');
        console.log("Sent Signal on socket ");
      }

      fs.writeFile( __dirname + "/" + "data.json", JSON.stringify(data, null, 4), function (err) {
        if (err) return console.log(err);
        });
      res.send(data.users[userId].estufas[ghId].actuators);
    });
  }else{
    res.send("ERROR: Greenhouse not available");
  }
})

// AUTO MODE STATE
app.post('/api/detailsUser/:userId([0-9]+)/listGH/:ghId([0-9]+)/auto/state=:stateVal([0-1])/', function(req, res, next) {
 console.log("Connection " + client.remoteAddress);
 console.log("Connection " + client.connecting);

  if (typeof client.remoteAddress !== "undefined") {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
      data = JSON.parse(data);
      var userId = req.params.userId;
      var ghId = req.params.ghId;
      var stateVal = req.params.stateVal;
      data.users[userId].estufas[ghId].auto = stateVal;
      console.log("AUTO MODE STATE: " + stateVal);

      if (stateVal == 1){
        client.write('7');
        console.log("Sent Signal on socket ");

      }else{
        client.write('8');
        console.log("Sent Signal on socket ");

      }

      fs.writeFile( __dirname + "/" + "data.json", JSON.stringify(data, null, 4), function (err) {
        if (err) return console.log(err);
      });
      res.send(data.users[userId].estufas[ghId].actuators);
    });
  }else{
    res.send("ERROR: Greenhouse not available");
  }
})

// POST TEMPERATURE VALUE
app.post('/api/detailsUser/:userId([0-9]+)/listGH/:ghId([0-9]+)/temperature=:temp([0-9]+)', function(req, res, next) {
 console.log("Connection " + client.remoteAddress);
 console.log("Connection " + client.connecting);

  if (typeof client.remoteAddress !== "undefined") {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
      data = JSON.parse(data);
      var userId = req.params.userId;
      var ghId = req.params.ghId;
      var temperature = req.params.temp;
      data.users[userId].estufas[ghId].auto_temperature = temperature;
      console.log("TEMPERATURE SENT: " + temperature);

      var tempS = ("v" + temperature);
      console.log(tempS);
      client.write(tempS);
      console.log("Sent Signal on socket ");
      //res.send(tempS);
      
      fs.writeFile( __dirname + "/" + "data.json", JSON.stringify(data, null, 4), function (err) {
        if (err) return console.log(err);
        });
      res.send(data.users[userId].estufas[ghId]);
    });
  }else{
    res.send("ERROR: Greenhouse not available");
  }

})

// POST AUTO WATERING VALUE
app.post('/api/detailsUser/:userId([0-9]+)/listGH/:ghId([0-9]+)/autoWatering=:autoTime([0-9]+)', function(req, res, next) {
 console.log("Connection " + client.remoteAddress);
 console.log("Connection " + client.connecting);

  if (typeof client.remoteAddress !== "undefined") {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
    data = JSON.parse(data);
    var userId = req.params.userId;
    var ghId = req.params.ghId;
    var watering = req.params.autoTime;
    data.users[userId].estufas[ghId].auto_watering = watering;
    console.log("WATERING TIME SENT: " + watering);

    var waterS = ("w" + watering);

    client.write(waterS);
    console.log("Sent Signal on socket ");
    //res.send(waterS);
    
    fs.writeFile( __dirname + "/" + "data.json", JSON.stringify(data, null, 4), function (err) {
      if (err) return console.log(err);
      });
      
    res.send(data.users[userId].estufas[ghId]);
    });
  }else{
    res.send("ERROR: Greenhouse not available");
  }

})

app.post('/api/updateGH/:userId([0-9]+)/:ghId([0-9]+)', function(req, res, next) {
  console.log("UPDATE FROM ARDUINO");
  if (typeof client.remoteAddress !== "undefined") {
    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
      data = JSON.parse(data);
      var userId = req.params.userId;
      var ghId = req.params.ghId;
      console.log(req.body);
      data.users[userId].estufas[ghId] = req.body;

      fs.writeFile( __dirname + "/" + "data.json", JSON.stringify(data, null, 4), function (err) {
      if (err) return console.log(err);
      });
    });
  }else{
    res.send("ERROR: Greenhouse not available");
  }
})




app.listen(8888);