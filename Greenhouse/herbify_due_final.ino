#include <Servo.h>
#include <Adafruit_Sensor.h>
#include "DHT.h"

#define DHTPIN 11     
#define DHTTYPE DHT11 
DHT dht(DHTPIN, DHTTYPE);

Servo servoWindow;  

const int WINDOW_OPEN = 180;
const int WINDOW_CLOSE = 5;

const int waterSensor = A0; 
const int lightSensor = A1; 
const int potpin = A2;

const int SERVO_PIN = 12;
const int BUZZERPIN = 9;
const int AUTOMATIONPIN= 5;
const int FANPIN = 6;
const int WATERPIN = 7;

int waterState = 0;
int lightState = 0;
int windowState = 0;
int fanState = 0;

int val;   
int ablePot = 0;
int automation = 0;

int autoTemp = 0;
int autoWater = 0;

void setup() {
  
  Serial.begin(9600);
  Serial.setTimeout(200);

  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(FANPIN, OUTPUT);
  pinMode(WATERPIN, OUTPUT);
  pinMode(AUTOMATIONPIN, OUTPUT);
  pinMode(BUZZERPIN, OUTPUT);
  
  dht.begin();
  servoWindow.attach(SERVO_PIN); 
  
  closeWindow();

}

void loop() {
  
  if(Serial.available() > 0){
    
    String s = Serial.readString();
    char letter = s.charAt(0);
    
    switch (letter) {
      case '0':
        lightOff();
        break;
      case '1':
        lightOn();
        break;
      case '2':
        //Serial.println("Close Window");
        closeWindow();
        break;
      case '3':
        //Serial.println("Open Window");
        openWindow();
        break;
      case '4':
        Serial.println(infoConcat());
        break;
      case '5':
        ablePot = 1;
        //Serial.println("KNOB enable");
        break;
      case '6':
        ablePot = 0;
        //Serial.println("KNOB disable");
        break;
      case '7':
        //Serial.println("AUTOMATION ON");
        automation = 1;
        break;
      case '8':
        //Serial.println("AUTOMATION OFF");
        automation = 0;
        break;
      case 'v':
        autoTemp = s.substring(1, s.length()-1).toInt();
        Serial.println(autoTemp);
        break;
      case 'w':
        autoWater = s.substring(1).toInt();
        //Serial.println(autoWater);
        break;
      case 'k':
        fanOn();
        break;
      case 'x':
        fanOff();
        break;
      case 'y':
        waterOn();
        break;
      case 'z':
        waterOff();
        break;
      default: 
        break;
    }
    
  }

  if(ablePot == 1){
    knobServo();
  }

  if(getWaterLevel() <= 1){
    tone(BUZZERPIN, 1000);
  }else{
    noTone(BUZZERPIN); 
  }
  
  if(automation == 1){
    delay(100);
    digitalWrite(AUTOMATIONPIN, HIGH);
    //Serial.println(autoTemp);
    //Serial.println(dht.readTemperature());
    if(dht.readTemperature() > autoTemp ){
      openWindow();
      fanOn();
    }else{
      closeWindow();
      fanOff();
    }
  }else{
    digitalWrite(AUTOMATIONPIN, LOW);
  }
  
}

String infoConcat(){
  String w = String(int(getWaterLevel() * 100 / 8));
  String l = String(int(getLightLevel() * 100 / 9));

  float dht[3];
  getHumTemp(&dht[0]);
  String h = String(int(dht[0]));
  String t = String(int(dht[1]));
  String hic = String(dht[2]);

  
  // tudo % excepto temperatura
  String result = "(" + w + "," + l + "," + h + "," + t + "," + hic + "," + automation + "," + waterState + "," + windowState + "," + fanState + "," + ablePot + "," + lightState +  ")";
  return result;

}

void knobServo(){
  val = analogRead(potpin);
  val = map(val, 0, 1023, 0, 180);
  servoWindow.write(val);
  delay(15);
}

void lightOn(){
  digitalWrite(LED_BUILTIN, HIGH);
  //Serial.println("LED ON");
  lightState = 1;
}

void lightOff(){
  digitalWrite(LED_BUILTIN, LOW);
  //Serial.println("LED OFF");
  lightState = 0;
}

void fanOn(){
  digitalWrite(FANPIN, HIGH);
  //Serial.println("FAN ON");
  fanState = 1;
}

void fanOff(){
  digitalWrite(FANPIN, LOW);
  //Serial.println("FAN OFF");
  fanState = 0;
}

void waterOn(){
  digitalWrite(WATERPIN, HIGH);
  Serial.println("WATER ON");
  waterState = 1;
}

void waterOff(){
  digitalWrite(WATERPIN, LOW);
  //Serial.println("WATER OFF");
  waterState = 0;
}


void openWindow() {
  servoWindow.write(WINDOW_OPEN);
  windowState = 1;
}

void closeWindow() {
  servoWindow.write(WINDOW_CLOSE);
  windowState = 0;
}


// 0 - Empty     8 - Full
int getWaterLevel() {

  int value = analogRead(waterSensor);
  
  if (value<=480){ 
    return 0;
  }
  else if (value>480 && value<=530){ 
    return 1; 
  }
  else if (value>530 && value<=615){ 
    return 2;
  }
  else if (value>615 && value<=660){ 
    return 3; 
  } 
  else if (value>660 && value<=680){ 
    return 4; 
  }
  else if (value>680 && value<=690){ 
    return 5; 
  }
  else if (value>690 && value<=700){ 
    return 6; 
  }
  else if (value>700 && value<=705){ 
    return 7; 
  }
  else if (value>705){ 
    return 8; 
  }
}

// 0 - Dark     8 - Sun
int getLightLevel() {
  
    int sensorValue = analogRead(lightSensor);
    int mapValue = sensorValue / 100 ;
    return mapValue;
}



void getHumTemp(float *result) {
  
  float h = dht.readHumidity();
  float t = dht.readTemperature();

  if (isnan(h) || isnan(t)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }
  float hic = dht.computeHeatIndex(t, h, false);
  result[0] = h;
  result[1] = t;
  result[2] = hic;
}

