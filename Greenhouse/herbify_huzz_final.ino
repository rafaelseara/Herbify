
#include <ESP8266WiFi.h>

const char* ssid     = "MEO-0EFB7E";
const char* password = "QMNFQ3E8ABA";
const char* ServerHost = "192.168.8.100";
const int ServerPort = 8888;

WiFiServer server(3300);

int status = WL_IDLE_STATUS;


void WiFiEvent(WiFiEvent_t event) {
    switch(event) {
        case WIFI_EVENT_STAMODE_GOT_IP:
            Serial.println("WiFi connected");
            Serial.println("IP address: ");
            Serial.println(WiFi.localIP());
            break;
        case WIFI_EVENT_STAMODE_DISCONNECTED:
            Serial.println("WiFi lost connection");
            break;
    }
}

void setup() {
    Serial.begin(9600);

    WiFi.disconnect(true);

    delay(1000);

    WiFi.onEvent(WiFiEvent);

    Serial.println();
    Serial.println();
    Serial.println("Wait for WiFi... ");
    
    while (status != WL_CONNECTED){
       Serial.print("Connecting to Network named: ");
       Serial.println(ssid);
       status = WL_CONNECTED;
       WiFi.begin(ssid, password);

       delay(1000);
    }
    server.begin();

}


void loop() {
  if (status != WL_CONNECTED)
  {
    Serial.println("NO CONNECTION");
    return;
  }

  WiFiClient client = server.available();
  if (!client)
  {
    sendMyInfoToServer();
    return;
  }

  
  Serial.println("Client connected");

  while (client.connected()){
    if(client.available() > 0){
      String s = client.readString();
      Serial.print(s);
      delay(500);
      if(Serial.available() > 0){
          String s = Serial.readString();
           Serial.println(s);
          if(s.charAt(0) == '('){
              Serial.println(s);
              sendSensorReads(s); 
          }
      }
    }
  }

  Serial.println("Client disonnected");
}


//(25,55,54,24.00,23.87,0,1,0,1,0,1)

void sendSensorReads(String data){
  //parse data
  data = data.substring(1, data.length()-2);
  Serial.println(data);

  // struct "(" + w + "," + l + "," + h + "," + t + "," + hic + "," + automation + "," + waterState + "," + windowState + "," + fanState + "," + ablePot + "," + lightState +  ")";

  int waterLvl = getValue(data, ',', 0).toInt();
  int lightPct = getValue(data, ',', 1).toInt();
  int humiPct = getValue(data, ',', 2).toInt();
  int tempC = getValue(data, ',', 3).toInt();
  float heatIndex = getValue(data, ',', 4).toFloat();
  int automation = getValue(data, ',', 5).toInt();
  int waterState = getValue(data, ',', 6).toInt();
  int windowState = getValue(data, ',', 7).toInt();
  int fanState = getValue(data, ',', 8).toInt();
  int ablePot = getValue(data, ',', 9).toInt();
  int lightState = getValue(data, ',', 10).toInt();
  
  WiFiClient client;
  
  if (!client.connect(ServerHost, ServerPort)) {
    Serial.println("connection failed to send values");
    return;
  }
  
  String url = "/api/updateGH/0/0";
  Serial.print("Requesting URL: ");
  Serial.println(url);

  String values = String("{\"light\": " + String(lightPct) + ",\"waterlvl\": " + waterLvl + ",\"temperature\": " + tempC + ",\"humidity\": " + humiPct + ",\"auto\": " + automation + ",\"actuators\": {\"water\": " + waterState + ",\"window\": " + windowState + ",\"fan\": " + fanState + ",\"mw\": " + ablePot + ",\"light\": " + lightState +"}}");

  client.println(String("POST ") + url + " HTTP/1.1");
  client.println(String("Host:  ") + String(ServerHost) );
  client.println("User-Agent: ArduinoHerbify/1.0");
  client.println("Connection: close");
  client.println("Content-Type: application/json");
  client.print("Content-Length: ");
  client.println(values.length());
  client.println();
  client.println(values);
 
  delay(500);
    
  while(client.available()){
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }
    delay(500);

}

String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

void sendMyInfoToServer(){

  WiFiClient client;
  
  if (!client.connect(ServerHost, ServerPort)) {
    Serial.println("connection failed to send WI-Fi Info");
    return;
  }
  
  // We now create a URI for the request
  String url = "/api/wifiId";
  Serial.print("Requesting URL: ");
  Serial.println(url);


  client.println(String("POST ") + url + " HTTP/1.1");
  client.println(String("Host:  ") + String(ServerHost) );
  client.println("User-Agent: ArduinoHerbify/1.0");
  client.println("Connection: close");
  client.println("Content-Type: text/plain; charset=UTF-8");
  client.print("Content-Length: ");
  client.println(WiFi.localIP().toString().length());
  client.println();
  client.println(WiFi.localIP().toString());
 
  delay(500);
    
  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
    String line = client.readStringUntil('\r');
    Serial.println(line);
  }
    delay(500);
}

